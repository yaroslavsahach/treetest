import scala.collection.mutable

case class Node(childNodes: List[Node], var leaves: List[Leaf]){
   def sortLeaves(leaves: List[Leaf]): List[Leaf] = {
     if (leaves.length <= 1) leaves
     else {
       val mid = leaves(leaves.length / 2)
       sortLeaves(leaves filter (x => mid.getWeight() > x.getWeight())) :::
       (leaves filter (x => mid.getWeight() == x.getWeight())) :::
       sortLeaves(leaves filter (x => mid.getWeight() < x.getWeight()))
     }
   }

  def splitByWeight(leaves: List[Leaf]): (List[Leaf], List[Leaf]) = {
    var acc = 0
    leaves.span(x => {
      acc = acc + x.getWeight()
      acc <= CustomTree.getW()
    })
  }

  def sortTree = {
    val queue = new mutable.Queue[Node]()
    var rest = List[Leaf]()

    // Traversal method - BFS
    queue.enqueue(this)
    while (queue.nonEmpty) {
      val currentNode = queue.dequeue()
      currentNode.leaves = currentNode.leaves ::: rest
      val processedLeaves = splitByWeight(sortLeaves(currentNode.leaves))
      currentNode.leaves = processedLeaves._1
      rest = processedLeaves._2
      if (currentNode.childNodes.nonEmpty) queue ++= childNodes
    }
  }

}

case class Leaf(weight: Int) {
  def getWeight(): Int = {
    this.weight
  }
}

object CustomTree {
  private var W = 3
  def getW(): Int = {
    this.W
  }
  def setW(newW: Int): Unit = {
    this.W = newW
  }
}