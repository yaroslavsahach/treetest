import org.scalatest.FunSuite

class TestSuite extends FunSuite {

  test("leaves sort test") {
    val tree = Node(Nil, List(Leaf(3), Leaf(5),Leaf(2),Leaf(1),Leaf(4)))
    tree.leaves = tree.sortLeaves(tree.leaves)
    assert(tree == Node(Nil, List(Leaf(1), Leaf(2),Leaf(3),Leaf(4),Leaf(5))))
  }

  test("split by weight test") {
    CustomTree.setW(6)
    val tree = Node(Nil, List(Leaf(3), Leaf(5),Leaf(2),Leaf(1),Leaf(4)))
    val result = tree.splitByWeight(tree.sortLeaves(tree.leaves))
    assert(result._1 == List(Leaf(1), Leaf(2), Leaf(3)))
    assert(result._2 == List(Leaf(4), Leaf(5)))
  }

  test("tree sort test #1 (from task)") {
    CustomTree.setW(3)
    val tree = Node(
      List(Node(Nil,Nil), Node(Nil,Nil), Node(Nil,Nil)),
      List(Leaf(2),Leaf(4),Leaf(3),Leaf(1))
    )
    tree.sortTree
    assert(tree == Node(
      List(Node(Nil, List(Leaf(3))), Node(Nil,Nil), Node(Nil,Nil)),
      List(Leaf(1),Leaf(2))
    ))
  }

  test("tree sort test #2 (moving leaves)") {
    CustomTree.setW(7)
    val tree = Node(
      List(Node(Nil,Nil), Node(Nil,Nil), Node(Nil,Nil)),
      List(Leaf(3),Leaf(5),Leaf(6),Leaf(7))
    )
    tree.sortTree
    assert(tree == Node(
      List(Node(Nil, List(Leaf(5))), Node(Nil, List(Leaf(6))),Node(Nil, List(Leaf(7)))),
      List(Leaf(3))
    ))
  }

  test("tree sort test #3 (simple sort)") {
    CustomTree.setW(10)
    val tree = Node(
      List(
        Node(Nil,List(Leaf(3),Leaf(4),Leaf(1),Leaf(2))),
        Node(Nil,List(Leaf(2),Leaf(1),Leaf(4),Leaf(3))),
        Node(Nil,List(Leaf(4),Leaf(2),Leaf(1),Leaf(3)))
      ),
      List(Leaf(2),Leaf(4),Leaf(3),Leaf(1))
    )
    tree.sortTree
    assert(tree == Node(
      List(
        Node(Nil,List(Leaf(1),Leaf(2),Leaf(3),Leaf(4))),
        Node(Nil,List(Leaf(1),Leaf(2),Leaf(3),Leaf(4))),
        Node(Nil,List(Leaf(1),Leaf(2),Leaf(3),Leaf(4)))
      ),
      List(Leaf(1),Leaf(2),Leaf(3),Leaf(4))
    ))
  }
}